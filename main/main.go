package main

import (
	"encoding/json"
	"fmt"
	"regexp"

	"gitlab.com/birowo/route"
)

func main() {
	z := make(route.Nds[int], route.ChrStSz)
	type Tbl struct {
		set, get string
		val      int
	}
	tbls := []Tbl{
		{"/a/:/b", "/a/123/b", 1},
		{"/b/:/c/:", "/b/234/c/345", 2},
		{"/a/:/b/c/*", "/a/345/b/c/456", 3},
	}
	for _, tbl := range tbls {
		z.Set(tbl.set, tbl.val)
	}
	bs, _ := json.MarshalIndent(z, "", "  ")
	re := regexp.MustCompile(`\s+{\s+"Chr": "",\s+"Str": "",\s+"Val": 0,\s+"Nds": null\s+},*`)
	bs = re.ReplaceAll(bs, []byte(""))
	println(string(bs))
	for _, tbl := range tbls {
		val, prms, n, wc := z.Get(tbl.get)
		fmt.Println("tbl:", tbl, ",prms:", prms[:n], ",wc:", wc, ",val:", val)
		if val != tbl.val {
			fmt.Println("error")
		}
	}
}
