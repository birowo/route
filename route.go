package route

type (
	Nd[T any] struct {
		Chr, Str string
		Val      T
		Nds      []Nd[T]
	}
	Nds[T any] []Nd[T]
)

const ChrStSz = 126

func PrmsWc[T any](str string, idx int, val T) (nds Nds[T], n int, v T) {
	n = len(str)
	for i := n - 1; i > idx; i-- {
		if str[i] == ':' {
			nds_ := make(Nds[T], ChrStSz)
			nds_[':'].Str = str[i+1 : n]
			n = i
			nds_[':'].Chr = ":"
			nds_[':'].Val = val
			val = v
			nds_[':'].Nds = nds
			nds = nds_
		} else if str[i] == '*' {
			nds = make(Nds[T], ChrStSz)
			n = i
			nds['*'].Chr = "*"
			nds['*'].Val = val
			val = v
		}
	}
	v = val
	return
}
func (nds Nds[T]) Set(str string, val T) {
	strLen := len(str)
	idx := 0
	for idx < strLen {
		chr := str[idx]
		idx++
		if nds[chr].Chr == "" {
			nds[chr].Chr = string(chr)
			nds_, n, v := PrmsWc(str, idx, val)
			nds[chr].Str = str[idx:n]
			nds[chr].Val = v
			nds[chr].Nds = nds_
			return
		}
		ndsChrStrLen := len(nds[chr].Str)
		i := 0
		for idx < strLen && i < ndsChrStrLen && str[idx] == nds[chr].Str[i] {
			idx++
			i++
		}
		if i == ndsChrStrLen {
			if idx == strLen {
				nds[chr].Val = val
				return
			}
			if nds[chr].Nds == nil {
				nds[chr].Nds = make(Nds[T], ChrStSz)
			}
			nds = nds[chr].Nds
			continue
		}
		nds_ := make(Nds[T], ChrStSz)
		if idx == strLen {
			chr_ := nds[chr].Str[i]
			nds_[chr_].Chr = string(chr_)
			nds_[chr_].Str = nds[chr].Str[i+1:]
			nds_[chr_].Val = nds[chr].Val
			nds_[chr_].Nds = nds[chr].Nds
			nds[chr].Str = nds[chr].Str[:i]
			nds[chr].Val = val
			nds[chr].Nds = nds_
			return
		}
		chr_ := nds[chr].Str[i]
		nds_[chr_].Chr = string(chr_)
		nds_[chr_].Str = nds[chr].Str[i+1:]
		nds_[chr_].Val = nds[chr].Val
		nds_[chr_].Nds = nds[chr].Nds
		chr_ = str[idx]
		nds_[chr_].Chr = string(chr_)
		nds__, n, v := PrmsWc(str, idx+1, val)
		nds_[chr_].Str = str[idx+1 : n]
		nds_[chr_].Val = v
		nds_[chr_].Nds = nds__
		nds[chr].Str = nds[chr].Str[:i]
		var v_ T
		nds[chr].Val = v_
		nds[chr].Nds = nds_
		return
	}
}
func (nds Nds[T]) Get(str string) (val T, prms [16]string, n int, wc string) {
	bgn := 0
	for nds != nil && bgn < len(str) {
		chr := str[bgn]
		if nds[chr].Chr == string(chr) {
			bgn++
			end := bgn + len(nds[chr].Str)
			if end <= len(str) && str[bgn:end] == nds[chr].Str {
				val = nds[chr].Val
				nds = nds[chr].Nds
				bgn = end
			} else {
				return
			}
		} else if nds[':'].Chr == ":" {
			end := bgn
			for end < len(str) && str[end] != '/' {
				end++
			}
			if n < 16 {
				prms[n] = str[bgn:end]
				n++
			} else {
				return
			}
			bgn = end
			end = bgn + len(nds[':'].Str)
			if end <= len(str) && str[bgn:end] == nds[':'].Str {
				val = nds[':'].Val
				nds = nds[':'].Nds
				bgn = end
			} else {
				return
			}
		} else if nds['*'].Chr == "*" {
			wc = str[bgn:]
			val = nds['*'].Val
			return
		}
	}
	return
}
